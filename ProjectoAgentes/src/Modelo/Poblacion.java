/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Random;

/**
 *
 * @author CltControl
 */
public class Poblacion extends Agente{
    
    static final double LIMITE = 0.1;
    static final double K = 2.3;
    static final double LEGITIMIDAD_GOBIERNO = 10;
    Random aleatorio = new Random();
    int fila;
    int columna;
    int campoDeVision;
    public static int numeroAgentes;
    public static int numeroPolicias;
    public static int Presos;
    public static int Rebeldes;
    static int Tranquilos;

    public Poblacion(){}
    
    public Poblacion(int fila, int columna, int campoDeVision) {
        this.fila = fila;
        this.columna = columna;
        this.campoDeVision = campoDeVision;
    }
    
    public static void buscarRadio(int x, int y, String tipo, String universo[][]){
        numeroPolicias = 0;
        numeroAgentes = 0;
        int radio=RebelionDePoblacion.radio;
        for (int x1=-radio;x1<radio+1;++x1){
            while((x+x1)<0){//condicion para no salir de matriz (arriba)
                x1++;
            }
            for (int y1=-radio;y1<radio+1;++y1){
                while((y+y1)<0){//condicion para no salir de matriz (izquierda)
                    y1++;
                }
                if(universo[x+x1][y+y1].equals(tipo)){
                    if (universo[x][y].equals("P")){              
                        universo[x+x1][y+y1]="P";
                        universo[x][y]="X";
                        numeroPolicias+=1;
                        numeroAgentes++;
                    }else{
                        numeroPolicias++;
                    }
                }//condicion para no salir de matriz (derecha)  
                while(!(y+y1+1<universo.length) && y1<radio+1){
                    y1++;
                }
            }//condicion para no salir de matriz (abajo)
            while(!(x+x1+1<universo.length) && x1<radio+1){
                x1++;
            }
        }
    }
    
    public void asignarPosicion(int densidad, String Tipo, boolean nuevo, String universo[][]
            , int tiempoDeCarcel, Policia policia[], Agente agente[]){
        // policias y agentes aleatorios en la matriz universo
        int fila;
        int columna;
        int TamUniverso = universo.length;
        System.out.println("Numero de (--"+Tipo+"--):"+densidad);
        int encarcelado = 0;        
        for(int i=0;i<densidad;i++){
            if (nuevo){
                fila=aleatorio.nextInt(TamUniverso);
                columna=aleatorio.nextInt(TamUniverso);
                if(universo[fila][columna].equals("X")){
                    universo[fila][columna]=Tipo;//colocar P o A en la posicion del policia o agente en la matriz
                    if(Tipo.equals("P")){
                        policia[i] = new Policia(fila, columna, campoDeVision, tiempoDeCarcel);//nuevo policia
                    }else{
                        agente[i] = new Agente(fila, columna, campoDeVision);// nuevo agente
                    }
                }else i--;
            }else{
                if(Tipo.equals("P")){
                    fila=(policia[i].fila-RebelionDePoblacion.radio+aleatorio.nextInt(RebelionDePoblacion.radio+2));
                    columna=(policia[i].columna-RebelionDePoblacion.radio+aleatorio.nextInt(RebelionDePoblacion.radio+2));
                   
                }else{
                    fila=(agente[i].fila-RebelionDePoblacion.radio+aleatorio.nextInt(RebelionDePoblacion.radio+2));
                    columna=(agente[i].columna-RebelionDePoblacion.radio+aleatorio.nextInt(RebelionDePoblacion.radio+2));
                }
            }
            if (encarcelado>100){
                i++;
                System.out.println("----------No hay una posicion disponible----------");
                encarcelado=0;
                
                
                
            }
        }      
    }

    @Override
    public String toString() {
        return "Poblacion{" + ", fila=" + fila + ", columna=" + columna + ", campoDeVision=" + campoDeVision + '}';
    }    
}
