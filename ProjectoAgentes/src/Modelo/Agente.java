/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import static java.lang.Math.exp;
import static java.lang.Math.round;

/**
 *
 * @author CltControl
 */
public abstract class Agente {
    public int perjuicioPercibido;
    public boolean rebelde = false;
    public boolean preso = false;
    protected int aversionAlRiesgo;
    protected double probabilidadDetencion;
    protected double agravio;    
    protected double riesgoNeto;

    public Agente(int fila, int columna, int campoDeVision) {
        //super(fila, columna, campoDeVision);
    }
    
    public void probabilidadDetencionEstimada (double k, int numP, int numA){
        aversionAlRiesgo = aleatorio.nextInt(2);
        probabilidadDetencion = 1 - exp(-k*round(numP/numA));
        riesgoNeto = aversionAlRiesgo * probabilidadDetencion;
    }

    public void actualizaAgentes(String[][] universo, int densAgente){
        
        Poblacion.buscarRadio(fila, columna, "P", universo);
        Poblacion.buscarRadio(fila, columna, "A", universo);
        aversionAlRiesgo = aleatorio.nextInt(2);
        probabilidadDetencion = 1 - exp(-K*round(numeroPolicias/numeroAgentes));
        riesgoNeto = aversionAlRiesgo * probabilidadDetencion;

        if ((agravio - riesgoNeto) > LIMITE){
            rebelde = true;            
        }else{
            rebelde = false;
        }
    }

    @Override
    public String toString() {
        return "AGENTE[" + "perjuicio=" + perjuicioPercibido + ", Riesgo=" + aversionAlRiesgo + ", agravio=" + agravio + ", riesgoNeto=" + riesgoNeto + ", probabilidadDetencion=" + probabilidadDetencion + ", rebelde=" + rebelde + ", preso=" + preso + ", fila=" + fila + ", columna=" + columna + ']';
    }   
}
